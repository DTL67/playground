module.exports = function (grunt, options) {

	var libsMapJs = {
		jquery:			['jquery/jquery.js'],
		bootstrap:		['bootstrap/js/bootstrap.js'],
		history:		['history/history.js'],
		bbq:			['bbq/bbq.js'],
		jAlerts:		['jAlerts/jquery.alerts.js'],
		toastmessage:	['jquery.toastmessage/js/jquery.toastmessage.js'],
		tinymce:		[
			'tinymce/preinit.js',
			'tinymce/tinymce.js'
		],
		chosen:			['chosen/chosen.jquery.js'],
		areYouSure:		['jquery.are-you-sure/jquery.are-you-sure.js'],
		autosize:		['autosize-master/jquery.autosize.js'],
		datetimepicker:	['bootstrap-datetimepicker/js/bootstrap-datetimepicker.js'],
		datepicker:		['bootstrap-datepicker/js/bootstrap-datepicker.js'],
		highcharts:		[
			'highcharts-4.1.5/highcharts.js',
			'highcharts-4.1.5/highcharts-more.js'
		],
		jvectormap:		[
			'jvectormap-1.2.2/jquery-jvectormap-1.2.2.min.js',
			'jvectormap-1.2.2/jquery-mousewheel.js',
			'jvectormap-1.2.2/jquery-jvectormap-world-mill-en.js'
		],
		timezone:		['jstz/jstz.js'],
		soundmanager:	['soundmanager/soundmanager2.js'],
		cookie:			['cookie/jquery.cookie.js'],
		localstorage:	['jquery-ajax-localstorage-cache/jquery-ajax-localstorage-cache.js'],
		visibility:		['visibility/jquery-visibility.js'],
		fineuploader:	[
			'fineuploader/js/header.js',
			'fineuploader/js/util.js',
			'fineuploader/js/button.js',
			'fineuploader/js/handler.base.js',
			'fineuploader/js/handler.form.js',
			'fineuploader/js/handler.xhr.js',
			'fineuploader/js/uploader.basic.js',
			'fineuploader/js/dnd.js',
			'fineuploader/js/uploader.js',
			'fineuploader/js/jquery-plugin.js',
			'fineuploader/js/custom.js'
		],
		fingerprint:	['fingerprint/fingerprint.js'],
		fingerprint2:	['fingerprint2/fingerprint2.js'],
		heatmap:		['heatmap/heatmap.js'],
		switcher:		['switcher/switcher.js'],
		flexslider:		['flexslider/jquery.flexslider.js'],
		starRating:		['bootstrap-star-rating/js/star-rating.min.js'],
		datelikephp:	['datelikephp/datelikephp.js'],
		autobahn:		['autobahn/autobahn.js'],
		imperavi:		[
			'imperavi-redactor/redactor.js',
			'imperavi-redactor/plugins/contexttoolbar/contexttoolbar.js',
			'imperavi-redactor/plugins/fontcolor/fontcolor.js',
			'imperavi-redactor/plugins/fontsize/fontsize.js',
			'imperavi-redactor/plugins/fontfamily/fontfamily.js',
			'imperavi-redactor/redactor.modal.js'
		],
		popoverx:		['bootstrapx-popoverx/js/bootstrapx-popoverx.js'],
		dropdown:		['dropdown/js/dropdown.js'],
		tutorialize:	[
			'tutorialize/js/jquery.tutorialize.js',
			'tutorialize/js/highlight.pack.js'
		],
		inputmask:		['jquery.inputmask/jquery.inputmask.bundle.js'],
		scrollbar:		['jquery.scrollbar/jquery.scrollbar.js'],
		dropbugo:		['drop-bugo/drop-bugo.js'],
		codehighlight:	['code_highlight/highlight.pack.js'],
		bootstrapslider: ['slider/js/bootstrap-slider.js'],
		colorpicker:	['bootstrap-colorpicker/dist/js/bootstrap-colorpicker.js'],
		webrtcadapter:	['webrtc-adapter/webrtc-adapter.js'],
		multiplyselect:	['multiply_select/multiply_select.js']
	};

	var packagesJs = {
		memberarea: [
			'lib:jquery',
			'lib:bootstrap',
			'lib:history',
			'lib:bbq',
			'lib:jAlerts',
			'lib:toastmessage',
			'lib:tinymce',
			'lib:chosen',
			'lib:areYouSure',
			'lib:autosize',
			'lib:datetimepicker',
			'lib:highcharts',
			'lib:jvectormap',
			'lib:timezone',
			'lib:soundmanager',
			'lib:cookie',
			'lib:localstorage',
			'lib:visibility',
			'lib:fineuploader',
			'lib:fingerprint',
			'lib:fingerprint2',
			'lib:heatmap',
			'lib:switcher',
			'lib:flexslider',
			'lib:starRating',
			'lib:datelikephp',
			'lib:autobahn',
			'lib:imperavi',
			'lib:popoverx',
			'lib:dropdown',
			'lib:tutorialize',
			'lib:inputmask',
			'lib:scrollbar',
			'lib:dropbugo',
			'lib:codehighlight',
			'lib:bootstrapslider',
			'lib:colorpicker',
			'lib:webrtcadapter',
			'lib:multiplyselect',

			'app:common/appmain.js',
			'app:common/apputils.js',
			'app:common/endlessscroll.js',
			'app:site.js',

			'app-widgets:wMenuLeft/assets/js/wMenuLeft.js',
			'app-widgets:wMenuTop/assets/js/wMenuTop.js',
			'app-widgets:wMenuLogo/assets/js/wMenuLogo.js',
			'app-widgets:wSimpleDrop/assets/js/wSimpleDrop.js',

			'settings-widgets:wAffiliateAgreements/assets/js/wAffiliateAgreements.js',

			'settings:general.js',
			'settings:seller.js',
			'settings:notifications.js',
			'settings:privacy.js',

			'forum:default.js'
		],

		guestarea: [
			'lib:jquery',
			'lib:bootstrap',
			'lib:history',
			'lib:bbq',
			'lib:jAlerts',
			'lib:toastmessage',
			'lib:autosize',
			'lib:highcharts',
			'lib:jvectormap',
			'lib:fingerprint',
			'lib:fingerprint2',
			'lib:timezone',
			'lib:localstorage',
			'lib:chosen',
			'lib:datetimepicker',
			'lib:heatmap',
			'lib:switcher',
			'lib:flexslider',
			'lib:tinymce',
			'lib:datelikephp',
			'lib:starRating',
			'lib:dropbugo',
			'lib:bootstrapslider',
			'lib:popoverx',

			'app:common/appmain.js',
			'app:common/apputils.js',
			'app:site.js',

			'app-widgets:wMenuTop/assets/js/wMenuTop.js',
			'app-widgets:wMenuLogo/assets/js/wMenuLogo.js',

			'settings:notifications.js',
			'forum:default.js'
		]
	};

	var packagesCss = {
		memberarea:		['memberarea-libs.css', 'memberarea.css'],
		guestarea:		['guestarea-libs.css', 'guestarea.css']
	};



	// Do not edit bellow code
	//////////////////////////
	function _buildConfig() {
		var result = {};

		// js
		function _getJsSrc(name) {
			var result = [];
			packagesJs[name].forEach(function(item) {
				var arr = item.split(':');
				if (arr[0] == 'lib') {
					libsMapJs[arr[1]].forEach(function(filepath) {
						result.push('../httpdocs/media/libs/' + filepath);
					});
				} else if (arr[0] == 'app') {
					result.push('assets/app-module/js/' + arr[1]);
				} else if (arr[0] == 'app-widgets') {
					result.push('components/widgets/' + arr[1]);
				} else {
					// module
					var alias = arr[0].split('-');
					if (alias[1] && alias[1]=='widgets') {
						// widgets
						result.push('modules/' + alias[0] + '/components/widgets/' + arr[1]);
					} else {
						result.push('modules/' + alias[0] + '/assets/js/' + arr[1]);
					}
				}
			});
			return result;
		}
		for (package in packagesJs) {
			result[package+'_js'] = {
				src: _getJsSrc(package),
				dest: '../httpdocs/media/js-dev/'+package+'.js',
				nonull: true,
				options: {separator: ";"},
			};
		}

		// css
		function _getCssSrc(name) {
			var result = [];
			packagesCss[name].forEach(function(item) {
				result.push('runtime/css-compiled/' + item);
			});
			return result;
		}
		for (package in packagesCss) {
			result[package+'_css'] = {
				src: _getCssSrc(package),
				dest: '../httpdocs/media/css-dev/'+package+'.css',
				nonull: true,
				options: {separator: ""},
			};
		}

		return result;
	}

	return _buildConfig();
};