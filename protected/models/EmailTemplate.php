<?php

class EmailTemplate extends ActiveRecord
{
	public function tableName()
	{
		return 'email_template';
	}

	public function relations()
	{
		return array(
			'idType' => array(self::BELONGS_TO, 'EmailType', 'id_type'),
		);
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

}