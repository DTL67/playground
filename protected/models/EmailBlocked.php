<?php

class EmailBlocked extends ActiveRecord
{
	public function tableName()
	{
		return 'email_blocked';
	}

	public function relations()
	{
		return array(
			'idType' => array(self::BELONGS_TO, 'EmailType', 'id_type'),
			'idUser' => array(self::BELONGS_TO, 'Users', 'id_user'),
		);
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


}