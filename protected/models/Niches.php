<?php

class Niches extends ActiveRecord
{
	public function tableName()
	{
		return 'niches';
	}

	public function rules()
	{
		return array(
		);
	}

	public function relations()
	{
		return array(
			'nicheUsers' => array(self::HAS_MANY, 'NicheUsers', 'id_niche'),
			'users'=>array(self::HAS_MANY, 'Users', array('id_user'=>'id'), 'through'=>'nicheUsers'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
		);
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function getUserNiches($id_user)
	{
		$model = Users::model()->with('niche')->find(array(
				'condition'=>'t.id = :id_user',
				'params'=>array(':id_user'=>$id_user),
				'order'=>'niche.name',
			));
		return $model->niche;
	}

	public static function getUserNichesComas($user)
	{
		if (is_numeric($user)) {
			$model = self::getUserNiches($user);
		} else {
			$model = $user;
		}
		$str = '';
		foreach ($model as $item) {
			$str .= ', '.$item->name;
		}
		return substr($str, 2);
	}
}
