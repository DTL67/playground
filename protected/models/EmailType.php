<?php

class EmailType extends ActiveRecord
{
	public function tableName()
	{
		return 'email_type';
	}

	public function relations()
	{
		return array(
			'idCategory' => array(self::BELONGS_TO, 'EmailCategory', 'id_category'),
			'idBlocked' => array(self::HAS_MANY, 'EmailBlocked', 'id_type'),
		);
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

}