<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>Error <?php echo $data['code']; ?></title>

	<style type="text/css">
		/*<![CDATA[*/
        html {overflow-y: scroll;}
		body {font-family: Arial; margin: 0; padding: 0;font-weight:normal;color:black;width: 100%;height: 100%;background: #fafafa;}
		h1 { font-family:"Verdana";font-weight:normal;font-size:18pt;color:red; padding-top: 0; margin-top: 0 }
		h2 { font-family:"Verdana";font-weight:normal;font-size:14pt;color:maroon }
		h3 {font-family:"Verdana";font-weight:bold;font-size:11pt}
		p {font-family:"Verdana";font-weight:normal;color:black;font-size:9pt;margin-top: -5px}
        .content_wrap {position: relative;width: 970px; margin: 0px auto;}
        .header {background-color: #e8e8e8;border-bottom:1px solid #fff}
		.inner {position: relative;width: 970px;margin: 0px auto;}
        .logo {float:left;}
        .logo img{margin: -4px 0 0}
        .content {width: 970px; margin: 0 auto;}
        .text {position: relative;padding: 35px;text-align: left}
        a {height: 50px;line-height: 50px;display: block;padding: 0 25px;}
        a:hover {background: #f0f0f0;}
        img {margin: 0;border:0;vertical-align:middle;padding-bottom: 2px;}
        .clearfix {clear:both;content: "";display: table;}
		/*]]>*/
	</style>
</head>

<body>
    <div class="content_wrap"></div>
    <div class="header">
        <div class="inner">
            <div class="logo">
                <a href="/">
                    <img src="/media/img/Udimi-buy-solo-logo-big-red.png" width="92" alt="Udimi.com">
                </a>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="content">
        <div class="text">
           <h1>Error <?php echo $data['code']; ?></h1>
           <h2><?php echo nl2br(CHtml::encode($data['message'])); ?></h2>
           <p>The above error occurred when the Web server was processing your request.</p>
        </div>
        <div class="clearfix"></div>
    </div>
</body>
</html>