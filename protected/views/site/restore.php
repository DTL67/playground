<?php
$this->metaTitle = 'Udimi. Restore password';
$this->jsInit('appSite', 'initRestore');
?>
<div class="app-site-auth">
	<div class="b-title">
		<div class="b-col-left">
			Restore password
		</div>
		<div class="b-col-right">
			<a href="<?php echo $this->createUrl(Yii::app()->user->loginUrl) ?>" class="ajax-get">Cancel</a>
		</div>
	</div>

	<div class="e-line"></div>

	<?php $form = $this->beginWidget(
		'CActiveForm', array(
			'id' => 'restore-form',
			'htmlOptions' => array(
				'class' => 'form-horizontal',
			),
		)
	); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model, 'passwordNew', array('class' => 'control-label col-sm-4 required-hide')); ?>
		<div class="col-sm-8">
			<?php echo $form->passwordField($model, 'passwordNew', array('class' => 'form-control')); ?>
			<div class="e-err"><?= $model->getError('passwordNew') ?></div>
		</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model, 'passwordNewConfirm', array('class' => 'control-label col-sm-4 required-hide')); ?>
		<div class="col-sm-8">
			<?php echo $form->passwordField($model, 'passwordNewConfirm', array('class' => 'form-control')); ?>
			<div class="e-err"><?= $model->getError('passwordNewConfirm') ?></div>
		</div>
	</div>

	<div class="b-btn">
		<button type="submit" class="btn btn-modern-primary ajax-post">
			Continue
		</button>
	</div>

	<?php $this->endWidget(); ?>
</div>