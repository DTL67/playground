<!DOCTYPE html>
<html>
<head>
	<?php
	$cs = Yii::app()->clientScript;
	$cs->registerMetaTag('text/html; charset=utf-8', null, 'Content-Type');
	$cs->registerMetaTag($this->metaKeywords, 'Keywords');
	$cs->registerMetaTag($this->metaDescription, 'Description');
	$cs->registerMetaTag(Yii::app()->getLanguage(), 'language');
	if ($this->favicon) echo CHtml::linkTag('shortcut icon', 'image/png', $this->favicon);
	if (isset($this->layoutPackageName)):
		$this->package();
	endif;
	$cs->registerScript('initApplication', "appMain.initApp(".CJavaScript::encode($this->jsAppOptions).");", CClientScript::POS_READY);

	if (!$this->skipFbOgTags) $this->fbOgTags();
	?>

	<title><?php echo $this->metaTitle ?></title>

</head>
<body>
<?php echo $content ?>
</body>
</html>