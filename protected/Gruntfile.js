
module.exports = function(grunt) {

	require('load-grunt-config')(grunt, {
		jitGrunt: true
	});

};
/*

module.exports = function(grunt) {
  require('jit-grunt')(grunt);

  grunt.initConfig({
    less: {
      development: {
        options: {
          compress: true,
          yuicompress: true,
          optimization: 2
        },
        files: {
          "./runtime/css-compiled/guestarea.css": "./assets/less/guestarea.less" // destination file and source file
		  ,"./runtime/css-compiled/memberarea.css": "./assets/less/memberarea.less" // destination file and source file
        }
      }
    }
  });

  grunt.registerTask('default', ['less']);
};
*/