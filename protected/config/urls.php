<?php
return array(
	'class'=>'application.components.UrlManager',
	'urlFormat'=>'path',
	'showScriptName'=>false,
	'caseSensitive'=>false,
	'modulesWithUrlRules'=>array('forum'),
	'rules'=>array(

		'' => array('site/index'),
		'<action:restore|ctrl>/key/<key:[\d]+>' => 'site/<action>',
		'confirm/<action:apply>/key/<key:[\d]+>' => 'confirm/<action>',

		'<action:login|forgot|logout|testimonials|signup>' => 'site/<action>',

		/* settings unsubscribe */
		'u/<hash:[a-z0-9]{16}>' => 'settings/notifications/instantunsubscribe',

		/* Short urls */
		'settings' => 'settings/general/index',
		
		/* Forum module */
		'forum' => 'forum/default/index',
		'forum/create' => 'forum/default/create',
		'forum/like' => 'forum/default/like',
		'forum/<slug:[\d]+-[\w-]+>' => 'forum/default/view',
	),

);