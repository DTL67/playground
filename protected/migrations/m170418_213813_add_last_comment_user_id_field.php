<?php
class m170418_213813_add_last_comment_user_id_field extends CDbMigration
{
	private $sql = array();

	public function up()
	{
		$this->sql[]=<<<SQL
ALTER TABLE `forum_posts`
ADD `last_comment_id_user` bigint(20) NULL;

SQL;

		$this->executeSql();
	}

	public function down()
	{
				$this->sql[]=<<<SQL
ALTER TABLE `forum_posts`
DROP COLUMN  last_comment_id_user;

SQL;

		$this->executeSql();

	}

	private function executeSql()
	{
		if (!empty($this->sql)) {
			foreach ($this->sql as $sql) {
				$this->execute($sql);
			}
		}
	}
}