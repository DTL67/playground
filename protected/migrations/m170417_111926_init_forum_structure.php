<?php
class m170417_111926_init_forum_structure extends CDbMigration
{
	private $sql = array();

	public function up()
	{
		$this->sql[]=<<<SQL
CREATE TABLE `forum_posts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_user` bigint(20) NOT NULL,
  `counter_views` int(11) DEFAULT 0,
  `counter_comments` int(11) DEFAULT 1,
  `last_comment_date` datetime DEFAULT NOW(),
  `dta_create` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `forum_comments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,  
  `id_post` bigint(20) NOT NULL,
  `id_user` bigint(20) NOT NULL,
  `is_first_comment` int(1) DEFAULT 0,
  `body` text NOT NULL,  
  `dta_create` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `forum_comment_likes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,  
  `id_comment` bigint(20) NOT NULL,
  `id_user` bigint(20) NOT NULL,
  `dta_create` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `forum_post_views` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,  
  `id_post` bigint(20) NOT NULL,
  `id_user` bigint(20) NOT NULL,
  `total_views` int(11) NOT NULL,
  `last_view_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


SQL;

		$this->executeSql();
	}

	public function down()
	{
		$this->sql[]=<<<SQL
		
DROP TABLE `forum_post_views`;

DROP TABLE `forum_comment_likes`;

DROP TABLE `forum_comments`;

DROP TABLE `forum_posts`;

SQL;

		$this->executeSql();
	}

	private function executeSql()
	{
		if (!empty($this->sql)) {
			foreach ($this->sql as $sql) {
				$this->execute($sql);
			}
		}
	}
}