<?php
class m170418_230422_create_forum_user_counters_table extends CDbMigration
{
	private $sql = array();

	public function up()
	{
		$this->sql[]=<<<SQL
CREATE TABLE `forum_user_counters` (
  `id_user` bigint(20) NOT NULL,
  `count_comments` int(11),
  `count_likes` int(11),
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
SQL;

		$this->executeSql();
	}

	public function down()
	{
		$this->sql[]=<<<SQL
		
DROP TABLE `forum_user_counters`;

SQL;

		$this->executeSql();
	}

	private function executeSql()
	{
		if (!empty($this->sql)) {
			foreach ($this->sql as $sql) {
				$this->execute($sql);
			}
		}
	}
}