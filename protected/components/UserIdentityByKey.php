<?php
class UserIdentityByKey extends UserIdentity
{
	const ERROR_USER_NOT_FOUND = 3;
	const ERROR_EXPIRED = 4;

	protected $login_key;
	protected $type;

	private $_id;

	public function __construct($login_key, $type)
	{
		$this->login_key = $login_key;
		$this->type = $type;
	}

	public function authenticate()
	{
		$criteria = new CDbCriteria();
		$criteria->with = array(
			'idUser' => array(
				'alias' => 'u',
				'select' => 'u.role, u.is_confirmed',
			),
		);
		$criteria->condition = '(`key`=:key) AND (`tp`=:type)';
		$criteria->params = array(':key' => $this->login_key, ':type' => $this->type);
		$model = UserIdentityKeys::model()->find($criteria);

		if (!$model || $model->idUser->role==Users::ROLE_DELETED) {
			$this->errorCode = self::ERROR_USER_NOT_FOUND;

		} elseif (strtotime($model->dta) < time()) {
			$this->errorCode = self::ERROR_EXPIRED;

		} else {
			$this->_id = $model->id_user;
			$this->errorCode = self::ERROR_NONE;
			$this->saveLoginToken($model->idUser);
		}

		return $this->errorCode == self::ERROR_NONE;
	}

	public function getId()
	{
		return $this->_id;
	}
}