<?php
class HtmlExist extends CValidator
{
	protected function validateAttribute($object,$attribute)
	{
		if ((!empty($object->$attribute)) && ($object->$attribute != MyString::strip_tags_smart($object->$attribute))) {
			$message=$this->message!==null?$this->message:Yii::t('app', '{attribute} should not contain HTML tags');
			$this->addError($object,$attribute,$message);
		}
	}
}