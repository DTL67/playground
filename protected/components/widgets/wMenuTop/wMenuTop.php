<?php

class wMenuTop extends Widget
{
	public $layoutPreviewMode;

	public function init()
	{
		parent::init(__CLASS__);
	}

	public function run()
	{
		if (Yii::app()->matchMask->check([
			'include'=>['*.site.signup'],
		])) {
			$items = ['login'];
		} else {
			$items = (Yii::app()->user->isGuest || $this->layoutPreviewMode)
				? ['login', 'signup']
				: ['support', 'affiliates', 'search'];
		}

		$items = MenuItem::loadItems($items, Yii::app()->user->model);

		$this->registerCallback('appWidgetsMenuTop', 'init');

		return $this->render('index', ['items'=>$items]);
	}

	public static function actions()
	{
		Yii::setPathOfAlias(__CLASS__, realpath(dirname(__FILE__)));
		return array(
			'reload'=>array('class'=>'application.components.WidgetBaseAction', 'widget_alias'=>__CLASS__),
		);
	}
} 