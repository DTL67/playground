<?php
class wFlashMessages extends CWidget
{
	public $options;

	public function run()
	{
		$cs = Yii::app()->getClientScript();
		$id = 0;
		$flashMessages = Yii::app()->user->getFlashes();

		if ($flashMessages) {
			foreach ($flashMessages as $type => $result) {
				if (strpos($type, '-')) {
					list($method, $type) = explode("-", $type);
				} elseif(in_array($type, array('success', 'error')))  {
					$method = 'toast';
				} else {
					$method = false;
				}
				if (is_array($result)) {
					$text = empty($result['text']) ? '' : $result['text'];
					$title = empty($result['title']) ? '' : $result['title'];
				} else {
					$text = $result;
					$title = 'Alert';
				}
				if ($method == 'jalert') {
					$cs->registerScript('FlowAlert_'.$id++, 'jAlert('.CJavaScript::encode($text).', '.CJavaScript::encode($title).')', CClientScript::POS_READY);
				} elseif ($method == 'toast') {
					if (!isset($this->options)) {
						throw new CException('Invalid params');
					}
					$this->options['text'] = $text;
					$this->options['type'] = $type;

					$cs->registerScript('FlowAlert_'.$id++, '$().toastmessage("showToast", '.CJavaScript::encode($this->options).')', CClientScript::POS_READY);
				} elseif ($method == 'score') {
					//$cs->registerScript('score_'.$id++, 'appMain.showScore('.CJavaScript::encode($text).', '.CJavaScript::encode($title).')', CClientScript::POS_READY);
				}
			}
		}
	}
}