<?php

class wMenuLeft extends Widget
{
	public $mobileIconClass;
	public $showMobileIcon;
	public $containerCssClass = 'js-menu-left-wrap';

	public function init()
	{
		parent::init(__CLASS__);
	}

	public function run()
	{
		if (Yii::app()->user->checkAccess(Users::ROLE_POWER)) {

			$arr = Yii::app()->user->checkAccess('prime_features') ? [] : ['activate'];
			$items = MenuItem::loadItems(array_merge(['mypage', 'solos', 'brokering', 'messages', 'money', 'swipes', 'links', 'cpa', 'settings', 'landing'], $arr), Yii::app()->user->model);

		} elseif (Yii::app()->user->checkAccess('prime_features')) {
			$items = MenuItem::loadItems(['mypage', 'solos', 'brokering', 'messages', 'money', 'swipes', 'links', 'settings'], Yii::app()->user->model);

		} else {
			$items = MenuItem::loadItems(['mypage', 'solos', 'brokering', 'messages', 'money', 'swipes', 'settings', 'activate'], Yii::app()->user->model);
		}

		return $this->render('index', ['items'=>$items]);
	}

	public static function actions()
	{
		Yii::setPathOfAlias(__CLASS__, realpath(dirname(__FILE__)));
		return array(
			'reload'=>array('class'=>'application.components.WidgetBaseAction', 'widget_alias'=>__CLASS__),
		);
	}
}