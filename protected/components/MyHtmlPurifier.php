<?php
class MyHtmlPurifier extends CHtmlPurifier
{
	public function purify($content) {
		$html = parent::purify($content);
		return str_replace('&amp;', '&', $html);
	}

	public function purifyUserText($content)
	{
		$this->options = array(
			'Core.AllowHostnameUnderscore'=>true,
			'AutoFormat.RemoveEmpty.RemoveNbsp'=>true,
			'AutoFormat.RemoveEmpty'=>true,
			'HTML.AllowedElements'=>'p,div,em,span,ol,li,ul,strong,br,a,hr,sub,sup,blockquote,code,pre,img,h1,h2,h3',
			'HTML.AllowedAttributes'=>array('*.style', 'a.href', 'img.src', 'img.width', 'img.height'),
			'URI.DisableExternalResources' => true,
		);
		return $this->purify($content);
	}
}