<?php

/**
 * ВАЖНО! Карта пунктов меню загружается в конструкторе MenuItem, в этот момент еще неясно какие пункты меню будут реально использоваться. Поэтому никакую динамику в этом методе использовать нельзя. Для реализации динамических вычислений используются спец приватные методы с case в классе MenuItem.
 */

class MenuMap extends CApplicationComponent
{
	private function _map()
	{
		return [
			'cart' => [
				'label' => '<i class="ud ud-shopping-cart"></i>',
				'url' => Yii::app()->createUrl('/solos/cart/index'),
				'htmlOptions' => [
					'data-scrollto' => 'top',
					'data-menu-name' => 'cart',
					'class' => 'ajax-get e-cart js-menu-cart-btn',
				],
				'active' => true,
				'liClass' => 'pull-left',
			],
			'buy' => [
				'label' => '<button class="btn btn-modern-blue">Buy</button>',
				'htmlOptions' => [
					'data-menu-name' => 'buy',
					'class' => 'e-buy js-menu-buy-btn',
				],
				'active' => true,
				'liClass' => 'pull-left',
			],
			'mypage_logo' => [
				'label' => CHtml::image('/media/img/Udimi-buy-solo-logo-big-red.png', 'Udimi', ['width'=>'96']),
				'url' => Yii::app()->createUrl('/mypage/general/index'),
				'htmlOptions' => [
					'class' => 'logo',
				],
				'active' => [
					'include' => [
						'mypage.*.*',
					],
				],
			],
			'mypage' => [
				'label' => Yii::t('app', 'Home'),
				'url' => Yii::app()->createUrl('/mypage/general/index'),
				'htmlOptions' => [
					'data-scrollto' => 'top',
					'data-menu-name' => 'mypage',
					'class' => 'ajax-get',
				],
				'active' => [
					'include' => [
						'mypage.*.*',
					],
				],
			],
			'solos' => [
				'label' => Yii::t('app', 'My solos'),
				'url' => Yii::app()->createUrl('/solos/general/index'), // has method filter 'dynamicUrl'
				'htmlOptions' => [
					'data-scrollto' => 'top',
					'data-menu-name' => 'solos',
					'class' => 'ajax-get',
				],
				'active' => [
					'include' => [
						'solos.*.*',
					],
				],
			],
			'brokering' => [
				'label' =>  Yii::t('app', 'Reseller'),
				'url' => Yii::app()->createUrl('/brokering/general/index'),
				'htmlOptions' => [
					'data-scrollto' => 'top',
					'data-menu-name' => 'brokering',
					'class' => 'ajax-get',
				],
				'active' => [
					'include' => [
						'brokering.*.*',
					],
				],
			],
			'links' => [
				'label' => Yii::t('app', 'Links'),
				'url' => Yii::app()->createUrl('/links/general/index'),
				'htmlOptions' => [
					'data-scrollto' => 'top',
					'data-menu-name' => 'links',
					'class' => 'ajax-get',
				],
				'active' => [
					'include' => [
						'links.*.*',
					],
				],
			],
			'money' => [
				'label' => Yii::t('app', 'Money'),
				'url' => Yii::app()->createUrl('/money/general/index'),
				'htmlOptions' => [
					'data-scrollto' => 'top',
					'data-menu-name' => 'money',
					'class' => 'ajax-get',
				],
				'active' => [
					'include' => [
						'money.*.*',
					],
					'exclude' => [
						'money.subscription.*',
					],
				],
			],
			'messages' => [
				'label' => Yii::t('app', 'Messages'),
				'url' => Yii::app()->createUrl('/messages/general/index'),
				'htmlOptions' => [
					'data-scrollto' => 'top',
					'data-menu-name' => 'messages',
					'class' => 'ajax-get',
				],
				'active' => [
					'include' => [
						'messages.*.*',
					],
				],
			],
			'swipes' => [
				'label' => Yii::t('app', 'Ad texts'),
				'url' => Yii::app()->createUrl('/settings/swipes/index'),
				'htmlOptions' => [
					'data-scrollto' => 'top',
					'data-menu-name' => 'swipes',
					'class' => 'ajax-get',
				],
				'active' => [
					'include' => [
						'settings.swipes.*',
					],
				],
			],
			'settings' => [
				'label' => Yii::t('app', 'Settings'),
				'url' => Yii::app()->createUrl('/settings/general/index'),
				'htmlOptions' => [
					'data-scrollto' => 'top',
					'data-menu-name' => 'settings',
					'class' => 'ajax-get',
				],
				'active' => [
					'include' => [
						'settings.*.*',
					],
					'exclude' => [
						'settings.swipes.*',
                        'settings.landing.*',
					],
				],
			],
			'activate' => [
				'label' => Yii::t('app', 'Upgrade To Prime'),
				'url' => Yii::app()->createUrl('/money/subscription/index'),
				'htmlOptions' => [
					'data-scrollto' => 'top',
					'data-menu-name' => 'activate',
					'class' => 'ajax-get',
				],
				'active' => [
					'include' => [
						'money.subscription.*',
					],
				],
				'liClass' => 'm-upgrade',
			],
			'search' => [
				'label' => Yii::t('app', 'Find Sellers'),
				'url' => Yii::app()->createUrl('/search'),
				'htmlOptions' => [
					'data-scrollto' => 'top',
					'data-menu-name' => 'search',
					'class' => 'ajax-get',
				],
				'active' => [
					'include' => [
						'search.*.*',
					],
				],
			],
			'affiliates' => [
				'label' => Yii::t('app', 'Affiliates'),
				'url' => Yii::app()->createUrl('/affiliates'),
				'htmlOptions' => [
					'data-scrollto' => 'top',
					'data-menu-name' => 'affiliates',
					'class' => 'ajax-get',
				],
				'active' => [
					'include' => [
						'affiliates.*.*',
					],
				],
			],
			'support' => [
				'label' => Yii::t('app', 'Help'),
				'url' => '/helper/pages/index',
				'htmlOptions' => [
					'data-scrollto' => 'top',
					'data-menu-name' => 'support',
					'class' => 'ajax-get',
				],
				'active' => [
					'include'=>[
						'support.*.*',
					],
				],
			],
			'index' => [
				'label' => CHtml::image('/media/img/Udimi-buy-solo-logo-big-black.png', 'Udimi', ['width'=>'101']),
				'url' => Yii::app()->createUrl('/site/index'),
				'htmlOptions' => [
					'class' => 'logo',
				],
				'active' => [
					'include' => [
						'app.site.*',
					],
				],
			],
			'login' => [
				'label' => Yii::t('app', 'Login'),
				'url' => Yii::app()->createUrl('/site/login', ['noautologin'=>0]),
				'htmlOptions' => [
					'data-scrollto' => 'top',
					'data-menu-name' => 'login',
					'class' => 'ajax-get',
				],
				'active' => [
					'include'=>[
						'app.site.login',
						'app.site.forgot',
						'app.site.restore',
					],
				],
			],
			'signup' => [
				'label' => Yii::t('app', 'Signup'),
				'url' => Yii::app()->createUrl('/site/signup', ['noautologin'=>0]),
				'htmlOptions' => [
					'data-scrollto' => 'top',
					'data-menu-name' => 'signup',
					'class' => 'ajax-get',
				],
				'active' => [
					'include'=>[
						'app.site.signup',
					],
				],
			],
			'tracker' => [
				'label' => Yii::t('app', 'Tracker'),
				'url' => Yii::app()->createUrl('/tracker/general/index'),
				'htmlOptions' => [
					'data-scrollto' => 'top',
					'data-menu-name' => 'tracker',
					'class' => 'ajax-get',
				],
				'active' => [
					'include' => [
						'tracker.*.*',
					],
				],
			],
			'cpa' => [
				'label' => Yii::t('app', 'CPA Network'),
				'url' => Yii::app()->createUrl('/cpa/search/index'),
				'htmlOptions' => [
					'data-scrollto' => 'top',
					'data-menu-name' => 'cpa',
					'class' => 'ajax-get',
				],
				'active' => [
					'include' => [
						'cpa.*.*',
					],
				],
			],

            'landing' => [
                'label' => Yii::t('app', 'Landing Page'),
                'url' => Yii::app()->createUrl('/landing/site/index'),
                'htmlOptions' => [
                    'data-scrollto' => 'top',
                    'data-menu-name' => 'settings',
                    'class' => 'ajax-get',
                ],
                'active' => [
                    'include' => [
                        'landing.*.*',
                    ],
                ],
            ],

			'divider' => [
				'liClass' => 'e-divider',
			],
		];
	}

	public function get($name)
	{
		$arr = $this->_map();

		$default = [
			'label' => null,
			'url' => null,
			'htmlOptions' => [],
			'active' => false,
			'liClass' => null,
			'visible' => true,
			'cntPrimary' => 0,
			'cntSlave' => 0,
			'cntPrimaryFormated' => 0,
			'cntSlaveFormated' => 0,
			'icon' => false,
			'isEmpty' => false,
			'isDenied' => false,
			'iconEmpty' => '<i class="e-icon-text">empty</i>',
			'iconDenied' => '<i class="e-icon-text">Prime only</i>',
			'children' => null,
		];

		foreach ($arr as $arrKey=>$arrVal) {
			foreach ($default as $defaultKey=>$defaultVal) {
				if (!isset($arr[$arrKey][$defaultKey])) {
					$arr[$arrKey][$defaultKey] = $default[$defaultKey];
				}
			}
		}

		if (empty($arr[$name])) {
			throw new CException('MenuMap item does not exist');
		}

		return $arr[$name];
	}
}