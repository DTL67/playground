<?php
/**
 * класс помощник для работы с файлами
 * обращение к методам MyFiles::functonName();
 * в конфиге в impot необходимо подключить импорт хелперов 'application.helpers.*'
 */
class MyFiles {
	/**
	 * @static Рекурсивное создание директорий
	 *
	 * @param string $folder Абсолютный путь от корня file system ('X:\' для windows и '/' для unix)
	 *
	 * @return bool
	 */
	public static function createFolder($folder)
	{
		$debug = !empty($_GET['debug']) || (!empty($argv) && array_search('debug', $argv) !== false);
		if (!file_exists($folder)) {
			$folder = str_replace('\\', '/', $folder);
			$folder = rtrim($folder, '/');
			$folder = preg_replace('#/+#', '/', $folder);
			if (strpos(strtolower(php_uname('s')), 'windows') !== false) {
				if ($debug) echo "Windows\n<br>";
				if (strpos($folder, ':') != 1) {
					throw new CException('Windows. Relation path not supported. Use absolute path');
				}
				$pref = substr($folder, 0, 2);
				$folder = substr($folder, 2);
			} else {
				if ($debug) echo "Unix\n<br>";
				if (strpos($folder, '/') != 0) {
					throw new CException('Unix. Relation path not supported. Use absolute path');
				}
				$pref = '';
			}

			// Поиск существующей начальной директории в пути, внутри которой нужно создавать
			// отсутствующие директории
			// Здесь же построение массива необходимых для создания директорий
			$foldersArr = explode("/", substr($folder, 1));
			$foldersArr = array_reverse($foldersArr);
			$startExistFolder = $folder;
			$needToCreateFolders = [];
			foreach ($foldersArr as $folderItem) {
				$startExistFolder = dirname($startExistFolder);
				$needToCreateFolders[] = $folderItem;
				if (empty($startExistFolder)) {
					throw new CException('Filesystem root reached');
				}
				if (file_exists($pref.$startExistFolder)) {
					break;
				}
			}
			$needToCreateFolders = array_reverse($needToCreateFolders);
			if ($debug) {
				echo var_export($startExistFolder, true);
				echo "\n<br>";
				echo var_export($needToCreateFolders, true);
				echo "\n<br>";
			}

			// Создание необходимых директорий
			$dir = $pref.$startExistFolder;
			foreach ($needToCreateFolders as $creatingFolder) {
				$dir .= '/'.$creatingFolder;
				if (!file_exists($dir)) {
					$res = mkdir($dir);
					if ($res === false) {
						throw new CException('Can not mkdir folder');
					}
					if ($debug) {
						echo "Created dir ".$dir."\n<br>";
					}
				}
				$res = chmod($dir, self::getChmod());
				if ($res === false) {
					throw new CException('Can not chmod folder');
				}
			}
			return true;
		}
		return true;
	}

	/**
	 * @static чтение содержимого директории
	 * @param $path - каталог, который нужно сканировать
	 *
	 * @return array - содержимое каталога
	 * @throws CException
	 */
	public static function readDirContent($path)
	{
		if (!is_dir($path))
			throw new CException('It is not a dir: '.$path);

		$files = array();
		if ($dir = @opendir($path)) {
			while (($file = readdir($dir)) !== false)
				if ($file[0] != '.')	// игнорим все, что начинается с точки (скрытые в Linux)
					$files[] = $file;
			closedir($dir);
		}
		return $files;
	}

	/**
	 * @static рекурсивная ф-я уаления каталога и\или его содержимого
	 * @param string $file каталог, который нужно удалить
	 * @param bool $del_self (false - удалить только содержимое|true - удалить содержимое и себя)
	 */
	public static function delFolderContent($file, $del_self = true)
	{
		if (file_exists($file))
		{
			chmod($file,self::getChmod());
			if (is_dir($file))
			{
				$handle = opendir($file);
				while ($filename = readdir($handle))
					if (!in_array($filename, array(".","..")))
						self::delFolderContent("$file/$filename");
				closedir($handle);
				if ($del_self) rmdir($file);
			}
			else unlink($file);
		}
	}

	/**
	 * получить максимально-допустимый размер загружаемого файла
	 * @return int
	 */
	public static function getSizeLimit()
	{
		$u = intval(ini_get('upload_max_filesize'))*1024*1024;
		$p = intval(ini_get('post_max_size'))*1024*1024;
		return $u > $p ? $p : $u;
	}

	/**
	 * удаление изображений удаляемой сущности, загруженых ч-з таб "Изображения" и текстовый редактор
	 * @param string $subCatalog - каталог. зачастую id контроллера. например products, media и т.д.
	 * @param int $id - id удаляемого элемента. например category, gallery и т.д.
	 *
	 * @throws CException
	 */
	public static function removeImages($subCatalog, $id)
	{
		// приводим тип id удаляемой сущности
		$id = (int)$id;

		// id удаляемой сущности и каталог не могут быть пустыми
		if (!$id OR !is_string($subCatalog) OR !$subCatalog)
			throw new CException('Argument not a dir');

		// на первой итерации будут из-я, загруженый через таб. на 2-ой ч-з редактор
		$folders = array('', 'materials/');
		foreach ($folders as $folder)
		{
			$dir = Yii::app()->config->get('homeDirPath')."/{$folder}{$subCatalog}/{$id}";
			if (is_dir($dir))
				self::delFolderContent($dir);
		}
	}

	/**
	 * создать пустую директорию
	 * @param string $folder
	 * @return bool
	 */
	public static function createEmptyFolder($folder)
	{
		self::createFolder($folder);
		self::delFolderContent($folder, false);
		return true;
	}

	/**
	 * @param string $file - путь/файл.расширение
	 */
	public static function removeFile($file)
	{
		if ($file AND is_file($file))
			unlink($file);
	}

	/**
	 * записать строку в файл
	 * @param string $file - путь/файл.расширение
	 * @param string $data - текст для записи
	 *
	 * @return bool
	 */
	public static function putIntoFile($file, $data)
	{
		if ($file = fopen($file, 'a+'))
		{
			if (fputs($file, $data))
			{
				fclose ($file);
				return true;
			} else return false;
		} else return false;
	}

	public static function curlFileGetContents($url)
	{
		$curl = curl_init();
		curl_setopt($curl,CURLOPT_URL, $url);
		curl_setopt($curl,CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl,CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0');
		curl_setopt($curl, CURLOPT_AUTOREFERER, false);
		curl_setopt($curl, CURLOPT_TIMEOUT, 10);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		$httpResponse = curl_exec($curl);

		if (!$httpResponse) {
			return '';
		}

		curl_close($curl);
		return $httpResponse;
	}

	function forceDownload($file) {
		if (file_exists($file)) {
			// сбрасываем буфер вывода PHP, чтобы избежать переполнения памяти выделенной под скрипт
			// если этого не сделать файл будет читаться в память полностью!
			if (ob_get_level()) {
				ob_end_clean();
			}
			// заставляем браузер показать окно сохранения файла
			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename=' . basename($file));
			header('Content-Transfer-Encoding: binary');
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Length: ' . filesize($file));
			// читаем файл и отправляем его пользователю
			readfile($file);
			exit;
		}
	}

/*------------------------ ХЕЛПЕРЫ ------------------------*/
	/**
	 * @static
	 * @return number текущий chmod
	 */
	private static function getChmod()
	{
		$chmod = empty(Yii::app()->params['chmode_folder']) ? 0777 : Yii::app()->params['chmode_folder'];
		return octdec($chmod);
	}
}