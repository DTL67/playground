<?php

class WebUser extends CWebUser
{
	const TELEPORT_DISTANCE = 1500000; //meters
	const TELEPORT_TIME_DIFF = 24; //hours
	const TELEPORT_TIMES_BAN = 5; //кол-во предупреждений перед баном.

	public $defaultReturnUrl;
	private $_model = null;
	private $_fromCtrl = false;
	private $_fpToken;
	private $_fpToken2;

	public $blockedCountries = [
		21=>'Bangladesh',
		163=>'Nigeria',
		169=>'Pakistan',
		4=>'Algeria',
		240 => 'Vietnam',
		52 => 'Congo',
		53 => 'Republic of the Congo',
		105 => 'Iran',
		106 => 'Iraq',
	];

	public function init()
	{
		// предотвращает автологин по кукам
		if (!empty($_GET['noautologin']) && $_GET['noautologin'] == 1) {
			$this->allowAutoLogin = false;
		}

		parent::init();
	}

	public function setAllowAutologin($val)
	{
		$this->allowAutoLogin = $val;
	}

	public function loginRequired()
	{
		if (Yii::app()->getRequest()->getIsAjaxRequest()) {
			header('Content-type: application/json');
			echo CJavaScript::jsonEncode(array(
					'redirect' => $this->loginUrl,
				));
			Yii::app()->end();
		} else {
			parent::loginRequired();
		}
	}

	public function beforeLogin($id, $states, $fromCookie)
	{
		if ($fromCookie && !empty($_GET['plx'])) {
			Yii::app()->end();
		}

		// удалим все expired токены логина по кукам
		UserIdentityKeys::deleteExpired($id);

		if ($fromCookie) {
			$userModel = Users::model()->findByPk($id);
			if (empty($userModel) || $userModel->role == Users::ROLE_DELETED) {
				return false;
			}
			// if this is login from cookie and fingerpring is empty
			if (empty($states['fpToken']) || empty($states['fpToken2'])) {
				return false;
			}
			// Если юзер логинится по кукам, сравниваем ключ из кук с ключем в БД
			if (empty($states['saved_key'])
				|| !UserIdentityKeys::model()->exists(
					'`key` = :key AND id_user = :user AND tp = :tp AND NOW() < DATE_ADD(dta, INTERVAL :ttl DAY)', [
						':user' => $id,
						':tp' => 'cookie_login',
						':ttl' => UserIdentityKeys::COOKIE_LOGIN_TTL,
						':key' => $states['saved_key']
					]
				)
			) {
				return false;
			}
		}

		$redirect_url = $this->getReturnUrl($this->defaultReturnUrl);
		if ($redirect_url == $this->loginUrl) {
			$redirect_url = $this->defaultReturnUrl;
		}
		$this->setReturnUrl($redirect_url);

		if (!empty($states['fpToken'])) {
			$this->_fpToken = $states['fpToken'];
		}
		if (!empty($states['fpToken2'])) {
			$this->_fpToken2 = $states['fpToken2'];
		}

		return true;
	}

	public function afterLogin($fromCookie)
	{
		if (!$userModel = Users::model()->findByPk($this->getId()))
			throw new CException('Model not found');

		if ($this->_fromCtrl || $this->getState('fromCtrl', NULL)) {
			// логинится админ из админки или это логин из куки после логина из админки
			Yii::app()->session->add('fromCtrl', true);
			$updateUserAttributes = array();

		} else {
			// логинится обычный юзер
			$updateUserAttributes = array(
				'ip_last_login' => $_SERVER['REMOTE_ADDR'],
			);

			// логирование удачных входов в систему
			$model = new LoginsLog;
			$model->id_user = $this->getId();
			$model->is_success = 1;
			$model->fingerprint = $this->_fpToken;
			$model->fingerprint2 = $this->_fpToken2;
			$model->save(false);

			// set read date for news
			$userConfig = new DUserConfig();
			$userConfig->id_user = $this->getId();
			$userConfig->set('previous_login_date', date('Y-m-d H:i:s'));
		}


		if ($fromCookie) {
			Yii::app()->session['FROM_COOKIE'] = true;
		}

		// update attributes
		if ($updateUserAttributes)
			Users::model()->updateByPk($this->getId(), $updateUserAttributes);

		return true;
	}

	public function beforeLogout()
	{
		$savedState = $this->getState('saved_key');
		if (!empty($savedState)) {
			UserIdentityKeys::model()->deleteAllByAttributes(
				array(
					'id_user' => $this->getId(),
					'tp' => 'cookie_login',
					'key' => $savedState
				)
			);
		}
		return parent::beforeLogout();
	}

	function getRole()
	{
		if ($user = $this->getModel()) {
			// в таблице User есть поле role
			return $user->role;
		} else {
			// this case is possible after rollback b/c of exception
			$this->logout(true);
			$redirect = Yii::app()->createUrl('/site/index');

			if (Yii::app()->request->getIsAjaxRequest()) {
				header('Content-type: application/json');
				echo CJavaScript::jsonEncode(array(
					'redirect' => $redirect,
				));
				Yii::app()->end();
			} else {
				Yii::app()->request->redirect($redirect);
			}
		}
	}

	public function getModel($refresh=false)
	{
		if ((!$this->isGuest && $this->_model === null) || $refresh){
			$this->_model = Users::model()->findByPk($this->id);
		}
		return $this->_model;
	}

	public function getModelArray()
	{
		$r = $this->getModel();
		$result = array();
		foreach ($r->attributes as $key=>$value) {
			$result[$key]=$value;
		}
		return $result;
	}

	public function setFromCtrl($fromCtrl)
	{
		$this->_fromCtrl = $fromCtrl;
	}

	public function setOnline()
	{
		if (!$this->isGuest) {
			$usersOnlineModel = UsersOnline::model()->findByAttributes(['id_user'=>$this->getId()]);
			$usersOnlineModel->is_online = 1;
			$usersOnlineModel->dta_login = date('Y-m-d H:i:s');
            $usersOnlineModel->setIP();
			$usersOnlineModel->save(false);
		}
	}

	public function getAfterLoginRedirectOptions()
	{
		return [
			'mypage'=>'My page',
			'solos'=>'My solos',
			'search'=>'Search',
			'affiliates'=>'Affiliates',
		];
	}
}