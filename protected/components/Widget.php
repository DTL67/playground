<?php
class Widget extends CWidget
{
	public $containerId;
	public $containerCssClass;
	public $returnHtml = false;
	public $identificator;
	private $class;
	public $tagWrapper = 'div';

    protected $_callbacks = [];

	public function init($class)
	{
		if ($this->containerId === null)
			$this->containerId = $class . '_' . $this->getId();
		$this->class = $class;
	}

	public function renderOriginal($view, $data = null)
	{
		return parent::render($view, $data, true);
	}

	public function render($view, $data = null)
	{
		if (is_array($view)) {
			$html = $view['html'];
		} else {
			$html = parent::render($view, $data, true);
		}

		$this->registerWidgetActions();

		if ($this->returnHtml)
			return $html;
		elseif ($this->tagWrapper)  {
			$options = array('id' => $this->containerId);
			if ($this->containerCssClass !== null) {
				$options['class'] = $this->containerCssClass;
			}
			$html = CHtml::tag($this->tagWrapper, $options, $html);
			echo $html;
		} else {
			echo $html;
		}
	}

	/**
	 * Зарегистрируем виджет в js объект приложения при условии что экшны виджета зарегистрированы.
	 */
	protected function registerWidgetActions()
	{
		$inst = $this->class;
		$actionsArray = $inst::actions();
		if (!empty($actionsArray)) {
			$obj = array(
				'url' => $this->controller->createUrl(strtolower($this->class)),
				'params' => array(
					'widgetmarker'=>1,
					'widgetparams'=>$this->getPublicProperties()
				)
			);
			$regname = empty($this->identificator) ? strtolower($this->class) : $this->identificator;
			Yii::app()->clientScript->registerScript('code_' . $this->containerId, 'appMain.registerWidget("'.$regname.'", '.CJavaScript::encode($obj).')', CClientScript::POS_READY);
		}
	}

	/**
	 * @return array Получим публичные свойства класса
	 */
	protected function getPublicProperties()
	{
		$result = array();
		$pp = new ReflectionObject($this);
		$props = $pp->getProperties(ReflectionProperty::IS_PUBLIC);
		foreach ($props as $prop) {
			$propName = $prop->getName();
			$result[$propName] = $this->$propName;
		}
		return $result;
	}

    public function registerCallback($obj, $method, $params=[])
    {
        $this->_callbacks[] = $obj . '.' . $method . '(' . CJavaScript::encode($params) . ')';
    }

    public function addCallback($item)
    {
        $this->_callbacks[] = $item;
    }

    public function getCallbacks()
    {
        return $this->_callbacks;
    }

}