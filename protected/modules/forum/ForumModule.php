<?php

class ForumModule extends WebModule
{
	public function init()
	{
		parent::init();
		$this->setImport(array(
			'forum.models.*',
			'forum.components.*',
		));
		//Yii::setPathOfAlias('wm', dirname(__FILE__).'/components/widgets');
	}
}
