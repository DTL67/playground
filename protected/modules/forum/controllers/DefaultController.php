<?php
class DefaultController extends Controller
{
	public function init()
	{
		if (!parent::init()) return false;
        $this->layout='//layouts/col1center';
		return true;
	}
    
    public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('deny',
				'users'=>array('?'),
			),
			array('deny',
				'roles'=>array(Users::ROLE_DELETED),
			),
		);
	}

	public function actionIndex()
	{
        $criteria = array(
            'with'=>array(
                'user',
                'comments'=>array(
                    'scopes'=>array('onlyFirstComments'),
                ),
                'lastCommentator'
            ),
        );
        /*
        if(!Yii::app()->user->isGuest)
        {
                $criteria['with']['postViews'] = array(
                    'condition'=>'postViews.id_user = :id_user',
                    'joinType'=>'LEFT JOIN',
                    'params'=> array(':id_user'=>Yii::app()->user->id),
                );
        }
        */

        $posts = ForumPost::model()->recenlty()->findAll($criteria);

        $postIds = array();
        foreach($posts as $item)
        {
            $postIds[] = $item->id;
        }

        $lastViews = ForumPostView::getViewForPosts($postIds,Yii::app()->user->isGuest ? NULL : Yii::app()->user->id);
        

        //VarDumper::dump($lastViews,10,true);

        $this->render('index',array('posts'=>$posts,'lastViews'=>$lastViews));
	}

    public function actionView($slug)
    {
        if(!$slug)
        {
            throw new CHttpException(404, 'Page not found');
        }

        $criteria = new CDbCriteria;
        $criteria->condition ='t.slug = :slug';
        $criteria->params = array(':slug'=>$slug);
        $criteria->with = array(
            //'user',
            'comments',
            'comments.user',
        );
        
        $post = ForumPost::model()->find($criteria);

        if(!$post)
        {
            throw new CHttpException(404, 'Post not found');
        }

        $modelComment = new ForumComment;
        if (isset($_POST[get_class($modelComment)])) {
			$modelComment->attributes = $_POST[get_class($modelComment)];
            
            $modelComment->id_post = $post->id;
            if (!$modelComment->validate()) {
				$this->appendJsonResponse([
					'error'=>MyUtils::getFirstError($modelComment),
				]);
			} else {

                $modelComment->save(false);

                ForumPost::updateFields($post->id,ForumPost::COUNTER_COMMENTS,array(
                    'last_comment_id_user'=>$modelComment->id_user,
                    'last_comment_date'=>$modelComment->dta_create,
                ));
                ForumUserCounter::updateFields(Yii::app()->user->id,ForumUserCounter::COUNTER_COMMENTS);

				$this->appendJsonResponse([
					'callback'=>'appMain.showToast("Comment added", "success")',
                    'soft_redirect'=> ''
				]);
			}

			$this->jsonResponse([]);
		}
        else
        {
            ForumPostView::addView($post->id);
        }

        // get likes for comments:
        $commentsIds = array();
        $usersIds = array();
        foreach($post->comments as $item)
        {
            $commentsIds[] = $item->id;
            $usersIds[$item->id_user] = 1;
        }
        $usersIds = array_keys($usersIds);
        $likeForComments = ForumCommentLike::getLikeForComments($commentsIds);

        //var_dump($usersIds);
        $userCounters = ForumUserCounter::getCountersForUsers($usersIds);
        
        //VarDumper::dump($userCounters,10,true);

        $this->render('view',array(
            'model'=>$post,
            'modelComment'=>$modelComment,
            'likeForComments'=>$likeForComments,
            'userCounters'=>$userCounters,
        ));
    }

    public function actionCreate()
	{
		$model = new ForumPost();
        $firstComment = new ForumComment;

		if (isset($_POST[get_class($model)])) {
			$model->attributes = $_POST[get_class($model)];

			if (!$model->validate()) {
				$this->appendJsonResponse([
					'error'=>MyUtils::getFirstError($model),
				]);
			} else {
                $firstComment->body = MyUtils::convert2db($model->body);
                $firstComment->is_first_comment = ForumComment::IS_FIRST_COMMENT_YES;
                $model->comments = array($firstComment);
                
                $model->withRelated->save(false,array('comments'));
                $model->generateSlug();
                ForumUserCounter::updateFields(Yii::app()->user->id,ForumUserCounter::COUNTER_COMMENTS);

				$this->appendJsonResponse([
					'callback'=>'appMain.showToast("Post success added.", "success")',
                    'soft_redirect'=>$this->createUrl('index')
				]);
			}

			$this->jsonResponse([]);
		}

		$this->render('create', ['model'=>$model]);

	}

    public function actionLike()
    {
        if(Yii::app()->request->isPostRequest)
        {
            $id_comment = Yii::app()->request->getPost('id_comment');
            if($id_comment)
            {
                $status = ForumCommentLike::like($id_comment);
                
                if($status == ForumCommentLike::STATUS_NEED_UP)
                {
                    $likeAvatarId = 'like-avatar-'.$id_comment.'-'.Yii::app()->user->id;
                    ob_start();
                    echo '<div class="forum-post-view-like-popover-content-item" id="'.$likeAvatarId.'">';
                    $this->widget('application.components.widgets.wAvatar.wAvatar', array(
                        'user'=>Yii::app()->user->getModel(),
                        'isOnlineShow'=>false,
                        'isOfflineShow'=>false,
                        'htmlOptions'=>array(
                            'width'=>50
                        ),
                    ));
                    echo '</div>';
                    $avatarHtml = ob_get_clean(); 
                    
                    $this->appendJsonResponse([
                        'callback'=>'forumLikes.likeUp('.$id_comment.',"'.addslashes($avatarHtml).'");',                    
                    ]);
                }
                else if($status == ForumCommentLike::STATUS_NEED_DOWN)
                {
                    $this->appendJsonResponse([
                        'callback'=>'forumLikes.likeDown('.$id_comment.','.Yii::app()->user->id.');',                    
                    ]);
                }                
            }
            else
            {
                $this->appendJsonResponse([
					'error'=>'Bad request (id_comment param not found)',
				]);
            }

            $this->jsonResponse([]);
        }
        else
        {
            throw new CHttpException(400, 'Bad request!');
        }
    }
}