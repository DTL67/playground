function ForumLikes() {

	this.options = {};

	var self = this;

    // отправить запрос на лайк/удаление лайка
    // при установке лайка - увеличить на 1 счетчик и добавить аватарку
    // при удалении - уменьшить на 1 счетчик и удалить аватарку

	this.init = function()
	{
        if(appMain.appOptions.idUser == null)
        {
            console.log('unauth user not handling.');
            return false;
        }
		$('.forum-post-view-like').bind('click', function(e) {
			/*if ($(this).hasClass('in-progress')) {
				//e.stopPropagation();
				e.preventDefault();
				return false;
			}
            console.log(e);*/
			/*$('.privacy-checkbox').each(function() {
				$(this).addClass('in-progress');
			});*/

			$(this).parent().find('form').find('.js-like-submit').click();
		});

	}

    this.likeUp = function(id_comment,avatarHtml){
        
        $('#forum-post-comment-'+id_comment+' form .js-like-submit').removeClass('in_progress_post');
        $('#forum-post-comment-'+id_comment+' .forum-post-view-like-popover-content').append(avatarHtml);
        
        var currentLikes = parseInt($('#forum-post-comment-'+id_comment+' .forum-post-view-like-count').html());
        $('#forum-post-comment-'+id_comment+' .forum-post-view-like-count').html(currentLikes+1);
        $('#forum-post-comment-'+id_comment+' .forum-post-view-like-popover-header-count').html(currentLikes+1);
    }

    this.likeDown = function(id_comment, id_user){
        $('#forum-post-comment-'+id_comment+' form .js-like-submit').removeClass('in_progress_post');
        $('#forum-post-comment-'+id_comment+' .forum-post-view-like-popover-content').find('#like-avatar-'+id_comment+'-'+id_user).remove();

        var currentLikes = parseInt($('#forum-post-comment-'+id_comment+' .forum-post-view-like-count').html());
        $('#forum-post-comment-'+id_comment+' .forum-post-view-like-count').html(currentLikes-1);
        $('#forum-post-comment-'+id_comment+' .forum-post-view-like-popover-header-count').html(currentLikes-1);
    }
}
var forumLikes = new ForumLikes();