<?php

class ForumPost extends ActiveRecord
{
    public $body;

    const COUNTER_VIEWS = 1;
    const COUNTER_COMMENTS = 2;    

	public function tableName()
	{
		return 'forum_posts';
	}

	public function rules()
	{
		return array(
            array('title','length','max'=>255),
            array('title, body', 'required'),
            array('body','length','max'=>65535),
            array('title','filter','filter'=>'MyUtils::convert2db', 'on'=>'insert'),            
		);
	}

	public function relations()
	{
		return array(
			'user'=>array(self::BELONGS_TO, 'Users', array('id_user')),            
            'comments'=>array(self::HAS_MANY, 'ForumComment', 'id_post','order'=>'comments.id ASC'),

            'postViews'=>array(self::HAS_MANY, 'ForumPostView', 'id_post'),
            'lastCommentator'=>array(self::BELONGS_TO, 'Users', array('last_comment_id_user')),    
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Subject',
            'body' => 'Comment',
		);
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    protected function beforeSave()
    {
        if(parent::beforeSave())
        {
            if($this->isNewRecord)
            {
                $this->slug = '';
                $this->id_user = Yii::app()->user->id;
                $this->dta_create = new CDbExpression('NOW()');
            }

            return true;
        }
        else
            return false;
    }

    public function generateSlug()
    {
        $title = MyUtils::transliterate($this->title);
        $prepareTitle = preg_replace (array("/[^\w]/","/[_]/"),"-",strtolower($title));

        $this->slug = $this->id .'-'.$prepareTitle;
        $this->update(array('slug'));
    }

    public function recenlty()
    {
        $this->getDbCriteria()->mergeWith(array(
            'order'=>'t.dta_create DESC',
        ));
        return $this;
    }

    public function getUrl()
    {
        return Yii::app()->createUrl('/forum/default/view',array(
            'slug'=>$this->slug,
        ));
    }

    public static function updateFields($id, $counterName, $params = array())
    {
        $attributes = array();
        $cmd = Yii::app()->db->createCommand();
        switch($counterName)
        {
            case self::COUNTER_VIEWS:
                $attributes['counter_views'] = new CDbExpression('(SELECT SUM(fpv.total_views) FROM forum_post_views as fpv WHERE fpv.id_post = :id_post)',array(
                    ':id_post'=>$id
                ));
                break;
            
            case self::COUNTER_COMMENTS:
                $attributes['counter_comments'] = new CDbExpression('(SELECT count(fc.id) FROM forum_comments as fc WHERE fc.id_post = :id_post)',array(
                    ':id_post'=>$id
                ));
                if(isset($params['last_comment_date']))
                {
                    $attributes['last_comment_date'] = $params['last_comment_date'];
                }
                else
                {
                    $attributes['last_comment_date'] = new CDbExpression('(SELECT fc.dta_create FROM forum_comments as fc WHERE fc.id_post = :id_post ORDER BY fc.dta_create DESC LIMIT 1)',array(
                        ':id_post'=>$id
                    ));
                }

                if(isset($params['last_comment_id_user']))
                {
                    $attributes['last_comment_id_user'] = $params['last_comment_id_user'];
                }
                
                break;
        }

        $cmd->update(self::model()->tableName(), $attributes, 'id=:id', array(':id'=>$id));

        //self::model()->updateByPk($id,$attributes);
    }
}
