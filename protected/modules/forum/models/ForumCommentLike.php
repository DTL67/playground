<?php

class ForumCommentLike extends ActiveRecord
{
	
	const STATUS_ERROR = 0;
	const STATUS_NEED_DOWN = 1;
	const STATUS_NEED_UP = 2;
	

	public function tableName()
	{
		return 'forum_comment_likes';
	}

	public function rules()
	{
		return array(
			array('id_comment','required'),
			array('id_comment','numerical'),
		);
	}
	public function relations()
	{
		return array(
			'user'=>array(self::BELONGS_TO, 'Users', array('id_user')),
            'comment'=>array(self::BELONGS_TO, 'ForumComment', array('id_comment')),
		);
	}

	public function attributeLabels()
	{
		return array();
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	protected function beforeSave()
    {
        if(parent::beforeSave())
        {
            if($this->isNewRecord)
            {
                $this->id_user = Yii::app()->user->id;
                $this->dta_create = new CDbExpression('NOW()');
            }

            return true;
        }
        else
            return false;
    }

	public static function like($id_comment)
	{
		$model = self::model()->find('id_user = :id_user AND id_comment = :id_comment',array(
			':id_user'=>Yii::app()->user->id,
			':id_comment'=>$id_comment,
		));

		$user_id = 0;

		if($model)
		{
			$id_user = $model->id_user;
			$model->delete();
			
			$status = self::STATUS_NEED_DOWN;
		}
		else
		{
			$model = new self;
			$model->id_comment = $id_comment;
			if($model->save())
			{
				$id_user = $model->id_user;
				$status = self::STATUS_NEED_UP;
			}
			else
			{
				$status = self::STATUS_ERROR;
			}
		}
		if($id_user)
		{
			ForumUserCounter::updateFields($id_user,ForumUserCounter::COUNTER_LIKES);
		}		

		return $status;
	}

	public static function getLikeForComments($commentsIds = array())
	{
		if(count($commentsIds)>0)
		{
			$criteria = new CDbCriteria(); 
			$criteria->addInCondition('id_comment',$commentsIds);
			$likes = self::model()->with('user')->findAll($criteria);

			
			if(count($likes))
			{
				$newArr = array();
				foreach($likes as $item)
				{
					$newArr[$item->id_comment][] = $item;
				}
				return $newArr;
			}
		}
		return null;		
	}
}