<?php

class ForumUserCounter extends ActiveRecord
{

    const COUNTER_COMMENTS = 1;
    const COUNTER_LIKES = 2;    

	public function tableName()
	{
		return 'forum_user_counters';
	}

	public function rules()
	{
		return array(
		);
	}

	public function relations()
	{
		return array(
		//	'user'=>array(self::BELONGS_TO, 'Users', array('id_user')),
        //    'post'=>array(self::BELONGS_TO, 'ForumPost', array('id_post')),
		);
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function updateFields($id, $counterName, $params = array())
    {
        $attributes = array();
		$model = self::model()->findByPk($id);
		if(!$model)
		{	
			$model = new self;
			$model->id_user = $id;
		}

		$cmd = Yii::app()->db->createCommand();
		switch($counterName)
		{
			case self::COUNTER_LIKES:
				$model->count_likes = new CDbExpression('(SELECT COUNT(fcl.id) FROM forum_comment_likes as fcl JOIN forum_comments as fc ON fc.id = fcl.id_comment WHERE fcl.id_user = :id_user)',array(
					':id_user'=>$id
				));
				break;
			
			case self::COUNTER_COMMENTS:
				$model->count_comments = new CDbExpression('(SELECT count(fc.id) FROM forum_comments as fc WHERE fc.id_user = :id_user)',array(
					':id_user'=>$id
				));                
				break;
		}
		$model->save();

		//$cmd->update(self::model()->tableName(), $attributes, 'id_user=:id', array(':id'=>$id));
		
        //self::model()->updateByPk($id,$attributes);
    }

	public static function getCountersForUsers($usersIds = array())
	{
		if(count($usersIds)>0)
		{
			$criteria = new CDbCriteria(); 
			$criteria->addInCondition('id_user',$usersIds);
			$models = self::model()->findAll($criteria);
			
			if(count($models))
			{
				$newArr = array();
				foreach($models as $item)
				{
					$newArr[$item->id_user] = $item;
				}
				return $newArr;
			}
		}
		return null;		
	}
}
