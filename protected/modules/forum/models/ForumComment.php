<?php

class ForumComment extends ActiveRecord
{
    //public $body;
    
    const IS_FIRST_COMMENT_NO = 0;
    const IS_FIRST_COMMENT_YES = 1;    

	public function tableName()
	{
		return 'forum_comments';
	}

	public function rules()
	{
		return array(
            array('body','filter','filter'=>'MyUtils::convert2db', 'on'=>'insert'),
            array('id_post, body','required'),
            array('id_post, id_user','numerical'),
            //array('body','unsafe','on'=>'update'),
		);
	}

	public function relations()
	{
		return array(
			'user'=>array(self::BELONGS_TO, 'Users', array('id_user')),
            'post'=>array(self::BELONGS_TO, 'ForumPost', array('id_post')),
            
            'likes'=>array(self::HAS_MANY, 'ForumCommentLike', array('id_comment')),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'body' => 'Comment',
            'dta_create' => 'Date create'
		);
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    protected function beforeSave()
    {
        if(parent::beforeSave())
        {
            if($this->isNewRecord)
            {
                $this->id_user = Yii::app()->user->id;
                $this->dta_create = new CDbExpression('NOW()');
            }

            return true;
        }
        else
            return false;
    }

    public function onlyFirstComments()
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition'=>'is_first_comment = :is_first_comment',
            'params'=>array(':is_first_comment'=>self::IS_FIRST_COMMENT_YES),
        ));
        return $this;
    }

    public static function getLastCommentsForPosts($postIds = array())
    {
        if(count($postIds)>0)
        {
            /*
            $cmd = Yii::app()->db->createCommand();
                $cmd->select(array('t.id','t.dta_create','t.id_user',))

            SELECT * FROM forum_comments AS t
            WHERE id =
            (SELECT MAX(id) FROM forum_comments AS t2 WHERE t.id_post = t2.id_post)
            ORDER BY t.id_post
            */
        }
    }
}
