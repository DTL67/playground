<?php

class ForumPostView extends ActiveRecord
{
	public function tableName()
	{
		return 'forum_post_views';
	}

	public function rules()
	{
		return array(
		);
	}

	public function relations()
	{
		return array(
			'user'=>array(self::BELONGS_TO, 'Users', array('id_user')),
            'post'=>array(self::BELONGS_TO, 'ForumPost', array('id_post')),
		);
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function addView($post_id)
	{
		$user_id = YIi::app()->user->isGuest ? 0 : Yii::app()->user->id;

		$model = self::model()->find('id_post = :id_post AND id_user = :id_user',array(
			':id_user'=>$user_id,
			':id_post'=>$post_id
		));

		if(!$model)
		{
			$model = new self();
			$model->id_user = $user_id;
			$model->id_post = $post_id;
			$model->total_views = 0;
		}
		$model->total_views++;
		$model->last_view_time = new CDbExpression('NOW()');

		$model->save();
		ForumPost::updateFields($post_id,ForumPost::COUNTER_VIEWS);
	}

	public static function getViewForPosts($postsIds = array(),$id_user = null)
	{
		if(count($postsIds)>0 && $id_user != null)
		{
			$criteria = new CDbCriteria(); 
			$criteria->compare('id_user',$id_user);
			$criteria->addInCondition('id_post',$postsIds);
			$views = self::model()->findAll($criteria);
			
			if(count($views))
			{
				$newArr = array();
				foreach($views as $item)
				{
					$newArr[$item->id_post] = $item->last_view_time;
				}
				return $newArr;
			}
		}
		return null;		
	}
}
