<?
    $disabledLikeFunction = Yii::app()->user->isGuest ? 'disabled' : '';
?>

<div class="forum-post-view-list-view" id="forum-post-comment-<?=$model->id?>">
    <div class="forum-post-view-list-view-left">
        <div class="">
            <?php $this->widget('application.components.widgets.wAvatar.wAvatar', array(
                'user'=>$model->user,
                'isOnlineShow'=>false,
                'isOfflineShow'=>false,
            )); ?>
           
            <span class="forum-post-view-list-view-left-counter"><?= $userCounters['count_comments'] ?: 0 ?></span> posts<br/>
            <span class="forum-post-view-list-view-left-counter"><?= $userCounters['count_likes'] ?: 0 ?></span> likes
        </div>
    </div>
    <div class="forum-post-view-list-view-right">
        <div class="forum-post-view-list-view-right-username">
            <?= $model->user->fullname ?>
        </div>
        <div class="forum-post-view-list-view-right-body">
            <?= $model->body ?>
        </div>
        <div class="forum-post-view-list-view-right-footer">
            <div class="forum-post-view-list-view-right-footer-right forum-post-view-like <?=$disabledLikeFunction?>">
                <span class="forum-post-view-like-label">Like</span> 
                <span class="forum-post-view-like-count"><?= count($likes) ?: 0 ?></span> &#x2764;
            </div>
            <div class="forum-post-view-like-popover">
                <div class="arrow"></div>
                <div class="forum-post-view-like-popover-header">
                    <span class="forum-post-view-like-popover-header-count"><?= count($likes) ?: 0 ?></span> likes
                </div>
                <div class="forum-post-view-like-popover-content">
                    <? if(count($likes))
                    {
                        foreach($likes as $likeItem)
                        {
                            $likeAvatarId = 'like-avatar-'.$likeItem->id_comment.'-'.$likeItem->id_user;
                            ?>
                            <div class="forum-post-view-like-popover-content-item" id="<?=$likeAvatarId?>">
                                <?php $this->widget('application.components.widgets.wAvatar.wAvatar', array(
                                    'user'=>$likeItem->user,
                                    'isOnlineShow'=>false,
                                    'isOfflineShow'=>false,
                                    'htmlOptions'=>array(
                                        'width'=>50
                                    ),
                                )); ?>
                            </div>
                            <?
                        }
                    }
                    ?>
                </div>
            </div>

            <form class="forum-post-view-like-form" action="<?=$this->createUrl('/forum/default/like')?>">
                <input type="hidden" name="id_comment" value="<?=$model->id?>">
                <a type="submit" href="<?=$this->createUrl('/forum/default/like')?>" class="ajax-post js-like-submit"></a>
            </form>
            <div class="forum-post-view-list-view-right-footer-left">
                <span class="forum-post-view-list-view-right-footer-left-date"><?= date('j M Y H:i',strtotime($model->dta_create)) ?> </span>
                <? if(!Yii::app()->user->isGuest):?>
                    <a href="#commentForm">Reply</a>
                <? endif; ?>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>