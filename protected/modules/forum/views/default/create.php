<?php //$this->beginContent('/layouts/main') ?>
<div class="forum-post-create">

	<?php $form = $this->beginWidget('CActiveForm', [
		'htmlOptions' => ['class' => 'form-horizontal']
	]) ?>

        <div class="forum-post-create-row">
            <div class="forum-post-create-row-label">
                <?php echo $form->labelEx($model,'title'); ?>
            </div>
            <div class="forum-post-create-row-field">
                <?php echo $form->textField($model,'title',array('class'=>'forum-post-field-title')); ?>
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="forum-post-create-row">
            <div class="forum-post-create-row-label">
                <?php echo $form->labelEx($model,'body'); ?>
            </div>
            <div class="forum-post-create-row-field">
                <?= $form->textArea($model, 'body', [
                    'id' => 'forum_post_body',
                    'class' => 'forum-post-field-body',
                ]) ?>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="forum-post-create-buttons">
            <a href="<?=$this->createUrl('/forum/default/index')?>" class="ajax-get btn js-forum-comment-submit">Cancel</a>
            <button type="submit" class="ajax-post btn btn-primary js-forum-comment-submit">Create</button>
        </div>
	<?php $this->endWidget() ?>



</div>
<?php //$this->endContent() ?>