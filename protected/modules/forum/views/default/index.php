<?php //$this->beginContent('/layouts/main') ?>
<div class="forum-post-index">
    <div class="forum-post-index-header">
        
        <div class="forum-post-index-header-title">
            <h2>Forum post list</h2>
        </div>
        <div class="forum-post-index-header-button">
            <?= MyHtml::link('Create post',array('/forum/default/create'),array('class'=>'ajax-get btn btn-primary')); ?>
        </div>
        <div class="clearfix"></div>
    </div>
    

    <div class="forum-post-index-list">
        <? if(count($posts)>0)
        {
            foreach($posts as $item)
            {
                $this->renderPartial('_index_view',array(
                    'model'=>$item,
                    'lastViews'=>isset($lastViews[$item->id]) ? $lastViews[$item->id] : NULL,                    
                ));
            }
        }
        else
        {
            ?>
            <div class="">
                There aren't any post yet. 
            </div>
            <?
        }
        ?>
    </div>
</div>
<?php //$this->endContent() ?>