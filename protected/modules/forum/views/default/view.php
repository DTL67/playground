<?php $this->jsInit('forumLikes', 'init') ?>
<div class="forum-post-view">
    <div class="forum-post-view-header">
        <div class="forum-post-view-header-backlink">
            <?= MyHtml::link('&larr; Back',array('/forum/default/index'),array('class'=>'ajax-get')); ?>
        </div>

        <div class="forum-post-view-header-title">
            <h1><?= $model->title;?></h1>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="forum-post-view-list">
        <? if(count($model->comments)>0)
        {
            foreach($model->comments as $item)
            {
                $this->renderPartial('_view_comment',array(
                    'model'=>$item,
                    'likes'=> $likeForComments != null && isset($likeForComments[$item->id]) ? $likeForComments[$item->id] : [],
                    'userCounters'=>isset($userCounters,$userCounters[$item->id_user]) ? $userCounters[$item->id_user] : NULL, 
                ));
            }
        }
        ?>
    </div>
    <div class="clearfix"></div>

    <? if(!Yii::app()->user->isGuest):?>
    <div class="forum-post-view-form" id="commentForm">
        <div>
            <h3>Add comment</h3>
        </div>
        <?php $form = $this->beginWidget('CActiveForm', [
            'htmlOptions' => ['class' => 'form-horizontal']
        ]) ?>

            <div class="forum-post-view-form-left">
                <?php $this->widget('application.components.widgets.wAvatar.wAvatar', array(
                    'user'=>Yii::app()->user->getModel(),
                    'isOnlineShow'=>false,
                    'isOfflineShow'=>false,
                )); ?>
            </div>

            <div class="forum-post-view-form-right">
                <div>
                    <?php// echo $form->labelEx($modelComment,'body'); ?>
                    <?= $form->textArea($modelComment, 'body', [
                            'id' => 'forum-comment-body',
                            'class' => 'forum-comment-body',                            
                        ]) ?>
                    <?php //echo $form->error($modelComment,'body'); ?>
                </div>
                <div class="forum-post-view-form-right-submit-button-container">
                    <button type="submit" class="ajax-post btn btn-primary js-forum-comment-submit">POST</button>
                </div>
            </div>
            <div class="clearfix"></div>
        <?php $this->endWidget() ?>
    </div>
    <? endif; ?>
</div>