<?php
    $countCommentsClass = '';
    //var_dump($lastViews,$model->last_comment_date, $model->last_comment_id_user);
    if($lastViews != NULL)
    {
        if($lastViews < $model->last_comment_date)
        {
            $countCommentsClass = 'have-unread-comment';
        }
    }
    else
    {
        if(!Yii::app()->user->isGuest && Yii::app()->user->id == $model->id_user)
        {

        }
        else
        {
            $countCommentsClass = 'have-unread-comment';
        }
    }
    //var_dump($lastViews, $model->last_comment_date);
?>

<div class="forum-post-index-list-view">
    <a href="<?=$model->getUrl()?>" class="ajax-get forum-post-index-list-view-link">
        <div class="forum-post-index-list-view-left">
            <?php $this->widget('application.components.widgets.wAvatar.wAvatar', array(
                'user'=>$model->user,
                'isOnlineShow'=>false,
                'isOfflineShow'=>false,
                'link'=>false,
            )); ?>
        </div>
        <div class="forum-post-index-list-view-right">
            <div class="forum-post-index-list-view-link-content">
                <div class="forum-post-index-list-view-link-content-right">
                    <div class="forum-post-index-list-view-link-content-right-counters">
                        <div class="forum-post-index-list-view-link-content-right-counters-views">
                            <span><?= $model->counter_views?></span>
                            <br/>
                            views
                        </div>
                        <div class="forum-post-index-list-view-link-content-right-counters-comments <?=$countCommentsClass?>">
                            <span><?= $model->counter_comments?></span>
                            <? if($countCommentsClass != '')
                            {
                                ?>
                                <span class="unread-comment-star">*</span>
                                <?
                            }
                            ?>
                            <br/>
                            replies
                        </div>
                        <div class="clearfix"></div>      
                    </div>
                    <div class="forum-post-index-list-view-link-content-right-last-commented">
                        last 
                        <? if($model->lastCommentator)
                        {
                            echo $model->lastCommentator->fullname;
                        }
                        else
                        {
                            echo $model->user->fullname;
                        }
                        ?>
                    </div>
                </div>
                <div class="forum-post-index-list-view-link-content-left">
                    <div class="forum-post-index-list-view-link-content-left-firstrow">
                        <span class="forum-post-index-list-view-link-content-left-firstrow-title">                            
                            <?= $model->title ?>
                        </span>
                        <span class="forum-post-index-list-view-link-content-left-firstrow-username">
                            by <?= $model->user->fullname ?>
                        </span>
                        <span class="forum-post-index-list-view-link-content-left-firstrow-date">
                            <?= date('j M Y H:i',strtotime($model->dta_create)) ?>
                        </span>
                    </div>
                    <div class="forum-post-index-list-view-link-content-left-body">
                        <?= $model->comments[0]->body ?>
                    </div>   
                </div>
                <div class="clearfix"></div>            
            </div>
        </div>
        <div class="clearfix"></div>
    </a>
    <div class="clearfix"></div>
</div>