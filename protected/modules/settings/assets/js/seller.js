function SettingsSeller() {

	this.options = {};

	var self = this;

	this.initGeneralSettings = function(options)
	{
		// seller mode on/off
		$('#ProfileSolo_is_active').on('change', function(e) {
			$('#general-settings-form-submit').click();
		});

		if (options.is_active == 1) {
			var clicksMaxDaily = $('#ProfileSolo_clicks_max_daily'),
				clicksMin = $('#ProfileSolo_clicks_min'),
				clicksMax = $('#ProfileSolo_clicks_max');

			// clicks daily
			self.rebuildMinOptions(clicksMax, clicksMaxDaily, options.clicksOptions);
			clicksMaxDaily.on('change', function(e) {
				self.rebuildMinOptions(clicksMax, clicksMaxDaily, options.clicksOptions);
				self.rebuildMinOptions(clicksMin, clicksMax, options.clicksOptions);
				$('#general-settings-form-submit').click();
			});


			// clicks max
			self.rebuildMinOptions(clicksMin, clicksMax, options.clicksOptions);
			clicksMax.on('change', function(e) {
				self.rebuildMinOptions(clicksMin, clicksMax, options.clicksOptions);
				$('#general-settings-form-submit').click();
			});

			// clicks min
			clicksMin.on('change', function(e) {
				$('#general-settings-form-submit').click();
			});


			// price per click min
			var clicksPriceMin = $('#ProfileSolo_click_price_min'),
				clicksPriceMax = $('#ProfileSolo_click_price_max');;

			self.rebuildMinOptions(clicksPriceMin, clicksPriceMax, options.clickPriceOptions);
			clicksPriceMax.on('change', function(e) {
				self.rebuildMinOptions(clicksPriceMin, clicksPriceMax, options.clickPriceOptions);
				$('#general-settings-form-submit').click();
			});

			// price per click max
			clicksPriceMin.on('change', function(e) {
				$('#general-settings-form-submit').click();
			});


			// init lock days
			self.initLockday(options);
		}
	};

	this.turnOnErrorCallback = function()
	{
		var $item = $('#ProfileSolo_is_active');
		$item.parents('form').removeClass('dirty');
		$item.val(0).trigger("chosen:updated");
	};

	this.initLockday = function(options)
	{
		// order_urgency
		var orderUrgency = function(index) {
			$('.js-order-urgency-hint').html(options.orderUrgency[index].hint);
		}
		orderUrgency($('#ProfileSolo_order_urgency').val());
		$('#ProfileSolo_order_urgency').on('change', function(e) {
			orderUrgency($(this).val());
		});

		$('.js-autosubmit').bind('change', function(e) {
			$('#general-settings-form-submit').click();
			$(this).parents('.b-row').find('.js-wd-cb').attr('disabled', true);
			//$('.js-wd-cb').attr('disabled', true);
		});

		var timezoneDrop = new AppWidgetSimpleDrop('.js-best-solo-timezone');
		timezoneDrop.init();
		timezoneDrop.onSelect(function(val, $input) {
			$('#general-settings-form-submit').click();
		});
	};

	this.rebuildMinOptions = function(attr_min, attr_max, options)
	{
		var maxStr = attr_max.val();
		var minStr = attr_min.val();

		var max = maxStr.indexOf(".") != -1 ? parseFloat(maxStr) : parseInt(maxStr);
		var min = minStr.indexOf(".") != -1 ? parseFloat(minStr) : parseInt(minStr);

		if (max < min) {
			$('option:selected', attr_min).removeAttr("selected");
			$('option[value="'+max+'"]', attr_min).attr("selected", "selected");
		}

		var cur;
		$('option', attr_min).each(function() {
			curStr = $(this).attr('value');
			cur = curStr.indexOf(".") != -1 ? parseFloat(curStr) : parseInt(curStr);
			if (cur > max && $('option[value="'+curStr+'"]', attr_min).length>0) {
				//alert (cur+' removed')
				$('option[value="'+curStr+'"]', attr_min).remove();
			}
		});
		$('option', attr_max).each(function() {
			curStr = $(this).attr('value');
			cur = curStr.indexOf(".") != -1 ? parseFloat(curStr) : parseInt(curStr);
			if (cur <= max && ($('option[value="'+curStr+'"]', attr_min).length == 0) && options[cur]) {
				attr_min.append('<option value="'+curStr+'">'+options[cur]+'</option>');
			}
		});
		attr_min.trigger("chosen:updated");
	};

	this.initNiches = function(options)
	{
		appUtils.extend(self.options, options);

		if ($('.niche-item-checkbox:checked').length >= self.options.nichesMax) {
			$('.niche-item-checkbox:not(:checked)').attr('disabled', true);
			$('.niche-item-checkbox:not(:checked)').parents('label').addClass('niche-label-disabled');
		}

		$('.niche-item-checkbox').bind('change', function() {
			$('#listniches-submit').click();
		});

	}
}
var settingsSeller = new SettingsSeller();