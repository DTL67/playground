<?php $this->jsInit('settingsSeller', 'initGeneralSettings', array(
	'clicksOptions' => $model->clicksOptions(),
	'clickPriceOptions' => $model->clickPriceOptions(),
	'clickPrice' => $model->click_price,
	'is_active' => $model->is_active,
	'orderUrgency'=>ProfileSolo::orderUrgency(),
));
$this->beginContent('/layouts/main') ?>
<div class="settings-seller-index">

	<div class="b-general">

		<?php $form = $this->beginWidget(
			'CActiveForm', array(
				'id' => 'general-settings-form',
				'htmlOptions' => array(
					'data-init' => 'dirty-check',
					'class' => 'form-horizontal',
				))) ?>

			<div class="b-title">
				<?php $this->widget('w.wGtitle.wGtitle', [
					'left'=>'Main',
					'right'=>[
						'content'=>MyUtils::BlogLink('how-to-sell', 'How to sell?'),
						'htmlOptions'=>['class'=>'r-title']
					],
				]); ?>
			</div>

			<div class="b-box-content">
				<div class="b-row">
					<div class="b-col-1">
						<?= $form->labelEx($model, 'is_active', array('class'=>'e-lbl')) ?>
					</div>
					<div class="b-col-2">
						<?= $form->dropDownList($model, 'is_active', $model->isActiveOptions(), array('class'=>'form-control'.($model->is_active ? ' bold-selected':''), 'data-init'=>'chosen-list', 'data-width'=>'77px')) ?>
					</div>
				</div>

				<?php if ($model->is_active): ?>
					<div class="b-row">
						<div class="b-col-1">
							<?= $form->labelEx($model, 'click_price', array('class'=>'e-lbl')) ?>
						</div>
						<div class="b-col-2">

							<?= $form->dropDownList($model, 'click_price_min', $model->clickPriceOptions(), array('class'=>'form-control', 'data-init'=>'chosen-list', 'data-width'=>'81px')) ?>
							<span>...</span>
							<?= $form->dropDownList($model, 'click_price_max', $model->clickPriceOptions(), array('class'=>'form-control', 'data-init'=>'chosen-list', 'data-width'=>'82px')) ?>
						</div>
					</div>

					<div class="b-row">
						<div class="b-col-1">
							<?= $form->labelEx($model, 'clicks_max', array('class'=>'e-lbl')) ?>
						</div>
						<div class="b-col-2">
							<?= $form->dropDownList($model, 'clicks_min', $model->clicksOptions(), array('class'=>'form-control', 'data-init'=>'chosen-list', 'data-width'=>'81px')) ?>
							<span>...</span>
							<?= $form->dropDownList($model, 'clicks_max', $model->clicksOptions(), array('class'=>'form-control', 'data-init'=>'chosen-list', 'data-width'=>'82px')) ?>
						</div>
					</div>

					<div class="b-row">
						<div class="b-col-1">
							<?= $form->labelEx($model, 'clicks_max_daily', array('class' => 'e-lbl')) ?>
						</div>
						<div class="b-col-2">
							<?= $form->dropDownList($model, 'clicks_max_daily', $model->clicksDailyOptions(), array('class' => 'form-control', 'data-init' => 'chosen-list', 'data-width'=>'182px')) ?>
						</div>
					</div>


					<div class="b-row">
						<div class="b-col-1">
							<?= $form->labelEx($model, 'order_urgency', array('class' => 'e-lbl')) ?>
						</div>
						<div class="b-col-2">
							<?= $form->dropDownList($model, 'order_urgency', $model->orderUrgencyOptions(), array('class' => 'form-control js-autosubmit', 'data-init' => 'chosen-list', 'data-width'=>'182px')) ?>
							<div class="e-hint js-order-urgency-hint"></div>
						</div>
					</div>

					<div class="b-row">
						<div class="b-col-1">
							<?= $form->labelEx($model, 'solos_number', array('class' => 'e-lbl')) ?>
						</div>
						<div class="b-col-2">
							<?= $form->dropDownList($model, 'solos_number', $model->solosNumberOptions(), array('class' => 'form-control js-autosubmit', 'data-init' => 'chosen-list', 'data-width'=>'182px')) ?>
						</div>
					</div>

					<div class="b-row">
						<div class="b-col-1">
							<?= $form->labelEx($model, 'best_solo_timezone', array('class' => 'e-lbl')) ?>
						</div>
						<div class="b-col-2 b-best-solo-time">
							<div class="float">
								<div class="b-col-left">
									<?php $this->widget('w.wSimpleDrop.wSimpleDrop', [
										'name'=>'ProfileSolo[best_solo_timezone]',
										'select'=>$model->best_solo_timezone ? $model->best_solo_timezone : ProfileSolo::TIMEZONE_UTC,
										'data'=>$model->bestSoloTimezoneOptions(),
										'htmlOptions'=>[
											'class'=>'js-best-solo-timezone',
										],
										'hideCaret'=>false,
										'dropPosition'=>'right',
									]) ?>
								</div>

								<div class="b-col-right">
									<input type="hidden" name="ProfileSolo[__best_solo_time]">

									<div class="b-hour-row">
										<?php for ($hour=0; $hour<12; $hour++): ?>
											<div class="cb-hour-wrapper">
												<input type="checkbox" id="cb-hours<?= $hour ?>" class="cbbtn-cb js-autosubmit js-wd-cb" name="ProfileSolo[__best_solo_time][]" value="<?=$hour;?>"<?= is_array($model->__best_solo_time) && in_array($hour, $model->__best_solo_time) ? ' checked="checked"' : '' ?>>
												<label for="cb-hours<?= $hour ?>" class="cbbtn-lbl">
													<?= str_pad($hour, 2, '0', STR_PAD_LEFT); ?>
													<span class="cbbtn-lock"><i class="ud-thumbs-up ud-blue"></i></span>
												</label>
											</div>
										<?php endfor; ?>
									</div>

									<div class="b-hour-row">
										<?php for ($hour=12; $hour<24; $hour++): ?>
											<div class="cb-hour-wrapper">
												<input type="checkbox" id="cb-hours<?= $hour ?>" class="cbbtn-cb js-autosubmit js-wd-cb" name="ProfileSolo[__best_solo_time][]" value="<?=$hour;?>"<?= is_array($model->__best_solo_time) && in_array($hour, $model->__best_solo_time) ? ' checked="checked"' : '' ?>>
												<label for="cb-hours<?= $hour ?>" class="cbbtn-lbl">
													<?= str_pad($hour, 2, '0', STR_PAD_LEFT); ?>
													<span class="cbbtn-lock"><i class="ud-thumbs-up ud-blue"></i></span>
												</label>
											</div>
										<?php endfor; ?>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="b-row">
						<div class="b-col-1">
							<label class="e-lbl">Lock week days</label>
						</div>
						<div class="b-col-2 b-week-days">
							<input type="hidden" name="ProfileSolo[__locked_week_days]">
							<?php foreach ($model->sellDaysOptions(true) as $key=>$val): ?>
								<div class="cbbtn-wrapper">
									<input type="checkbox" id="cb<?= $key ?>" class="cbbtn-cb js-autosubmit js-wd-cb" name="ProfileSolo[__locked_week_days][]" value="<?= $key ?>"<?= is_array($model->__locked_week_days) && in_array($key, $model->__locked_week_days) ? ' checked="checked"' : '' ?>>
									<label for="cb<?= $key ?>" class="cbbtn-lbl"><?= $val ?><span class="cbbtn-lock"><i class="fa fa-lock"></i></span></label>
								</div>
							<?php endforeach ?>
						</div>
					</div>

					<div class="b-row">
						<div class="b-col-1">
							<label class="e-lbl">List niches</label>
						</div>
						<div class="b-col-2">
							<?php $this->widget('wm.wNiches.wNiches'); ?>
						</div>
					</div>

				<?php endif ?>
				<button type="submit" id="general-settings-form-submit" class="btn btn-primary ajax-post hidden">Save</button>
			</div>
		<?php $this->endWidget() ?>
	</div>

	<?php if ($model->is_active): ?>
		<?php $this->widget('wm.wAffiliateAgreements.wAffiliateAgreements') ?>
	<?php endif ?>

</div>
<?php $this->endContent() ?>