<?php $this->jsInit('settingsNotifications', 'init') ?>

<?php $this->beginContent('/layouts/main') ?>
<div class="settings-notifications">

		<?php foreach ($models as $category): ?>

			<?php if ($category->sname == 'seller' && !Yii::app()->user->isGuest && !Yii::app()->user->model->profileSolos->is_active) continue ?>

			<?php $form = $this->beginWidget(
				'CActiveForm', [
				'htmlOptions' => [
					'class' => 'form-horizontal',
				]]) ?>
				<input type="hidden" name="id_category" value="<?= $category->id ?>">

				<div class="block-section">

					<div class="b-title">
						<?php $this->widget('w.wGtitle.wGtitle', [
							'left'=>$category->name,
						]) ?>
					</div>

					<div class="form-group">
						<?php foreach ($category->idType as $type): ?>
							<div class="col-sm-10 col-sm-offset-2">
								<div class="checkbox checkbox-primary">
									<?= CHtml::checkBox(
										'emailTypes[]', !isset($type->idBlocked[0]), [
										'id' => 'blocked_' . $type->id,
										'value' => $type->id,
										'class' => 'email-types-checkbox',
									]) ?>
									<label class="checkbox-skip-color" for="blocked_<?= $type->id ?>"><?= $type->name ?></label>
								</div>
							</div>
						<?php endforeach ?>
					</div>
				</div>

				<button type="submit" class="ajax-post hidden js-email-types-submit"></button>
			<?php $this->endWidget() ?>

		<?php endforeach ?>


	<?php if (!Yii::app()->user->isGuest): ?>
		<div class="block-section">
			<div class="b-title">
				<?php $this->widget('w.wGtitle.wGtitle', [
					'left'=>'Sound Notifications',
				]) ?>
			</div>
			<?php $this->widget('wm.wSound.wSound') ?>
		</div>
	<?php endif ?>

</div>
<?php $this->endContent() ?>