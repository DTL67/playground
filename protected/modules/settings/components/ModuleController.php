<?php

class ModuleController extends Controller
{
	public function init()
	{
		if (!parent::init()) return false;
		$this->metaTitle = 'Udimi. Settings';
		return true;
	}

	public function getTabs()
	{
		$c = $this->id;
		$a = $this->action->id;

		return array(
			array(
				'label'=>'General',
				'url'=>$this->createUrl('general/index'),
				'active'=>$c == 'general',
				'linkOptions'=>array('class'=>'ajax-get')
			),
			array(
				'label'=>'Seller Setup',
				'url'=>$this->createUrl('seller/index'),
				'active'=>$c == 'seller' || $c == 'apps',
				'linkOptions'=>array('class'=>'ajax-get')
			),
			array(
				'label'=>'Notifications',
				'url'=>$this->createUrl('notifications/index'),
				'active'=>$c == 'notifications',
				'linkOptions'=>array('class'=>'ajax-get')
			),
			array(
				'label'=>'Privacy',
				'url'=>$this->createUrl('privacy/index'),
				'active'=>$c == 'privacy',
				'linkOptions'=>array('class'=>'ajax-get')
			),
		);
	}
} 