<?php $form = $this->beginWidget(
	'CActiveForm', array(
		'id' => 'sound-notification-form',
		'htmlOptions' => array(
			'class' => 'form-horizontal',
		),
	)) ?>

	<div class="form-group">
		<div class="col-sm-10 col-sm-offset-2">
			<div class="checkbox checkbox-primary">
				<?= $form->checkBox($model, 'isSoundNewMessage', array('class'=>'sound-notification-item-checkbox', 'id'=>'isSoundNewMessage')) ?>
				<label class="checkbox-skip-color" for="isSoundNewMessage"><?= $model->getAttributeLabel('isSoundNewMessage') ?></label>
			</div>
		</div>

		<?php if (Yii::app()->user->model->profileSolos->is_active): ?>
			<div class="col-sm-10 col-sm-offset-2">
				<div class="checkbox checkbox-primary">
					<?= $form->checkBox($model, 'isSoundNewProposal', array('class'=>'sound-notification-item-checkbox', 'id'=>'isSoundNewProposal')) ?>
					<label class="checkbox-skip-color" for="isSoundNewProposal"><?= $model->getAttributeLabel('isSoundNewProposal') ?></label>
				</div>
			</div>
		<?php endif ?>

	</div>

	<button type="submit" class="ajax-post hidden" data-href="<?= $this->controller->createUrl('wsound.save')?>" data-widget="wsound" id="sound-notification-submit"></button>

<?php $this->endWidget() ?>
