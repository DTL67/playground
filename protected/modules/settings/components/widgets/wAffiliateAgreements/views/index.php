<?php if ($profileSolo->is_kent): ?>
	<?php $this->controller->jsInit('settingsWidgetAgreements', 'init') ?>
	<div class="b-custom-agr">
		<div class="b-title">
			<?php $this->controller->widget('w.wGtitle.wGtitle', [
				'left'=>'Custom Affiliates',
				'right'=>[
					'content'=>MyUtils::BlogLink('custom-affiliate-agreements', 'How this works?'),
					'htmlOptions'=>['class'=>'r-title']
				],
			]) ?>
		</div>

		<div class="b-box-content">
			<div class="e-note">Give selected affiliates higher commission.</div>
			<?php foreach ($models as $item):?>
				<?=$this->controller->renderPartial('application.modules.settings.components.widgets.wAffiliateAgreements.views.partial.partner_item', ['user'=>$item->idPartner, 'uid'=>$item->uid, 'percent'=>$item->percent], true) ?>
			<?php endforeach ?>

			<div class="new-agr-container" id="new-agr-container">
				<span class="ajax-get-modal" data-href="<?=$this->controller->createUrl('waffiliateagreements.create') ?>" data-widget="waffiliateagreements">Create new agreement</span>
			</div>

		</div>
	</div>
<?php endif ?>