<?php
class Save extends WidgetBaseAction
{
	public function run()
	{
		if (isset($_POST['after_login_redirect'])) {

			if (in_array($_POST['after_login_redirect'], array_keys(Yii::app()->user->getAfterLoginRedirectOptions()))) {
				Yii::app()->userConfig->set('after_login_redirect', $_POST['after_login_redirect']);
			}

			$widget = $this->getWidgetInstance();
			// Response
			$this->controller->jsonResponse(
				array(
					'container' => '#'.$widget->containerId,
					'content' => $widget->run(),
					'callback' => array(
						'appMain.showToast("Settings saved", "success")',
					),
				)
			);
		}
	}
}