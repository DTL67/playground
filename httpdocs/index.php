<?php
if (!empty($_SERVER['HTTP_X_SUCURI_CLIENTIP'])) {
    $_SERVER['REMOTE_ADDR'] = $_SERVER['HTTP_X_SUCURI_CLIENTIP'];
}
$isLocalIp = isset($_SERVER['REMOTE_ADDR'])
	&& isset($_SERVER['SERVER_ADDR'])
	&& ($_SERVER['REMOTE_ADDR'] == '127.0.0.1' OR $_SERVER['SERVER_ADDR'] == '::1');

$isLocalConfig = getenv('LOCAL_MODE');

if ($isLocalIp || $isLocalConfig) {
	defined('YII_DEBUG') or define('YII_DEBUG', true);
	defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',0);
}
else {
	defined('YII_DEBUG') or define('YII_DEBUG',false);
	defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);
}

require_once(dirname(__FILE__).'/../protected/vendor/autoload.php');

require_once(dirname(__FILE__).'/../framework/yii.php');

Yii::$enableIncludePath = false;

require_once(dirname(__FILE__).'/../protected/components/WebApplication.php');

Yii::setPathOfAlias('files', realpath(dirname(__FILE__).'/../files/'));

Yii::createApplication('WebApplication', dirname(__FILE__).'/../protected/config/main.php')->run();